# Life expectancy data science project

Project to examine life expectancy in different countries to experiment with Python and Rust.

* Ingest

[World Bank](https://data.worldbank.org/indicator/SP.DYN.LE00.IN)

* EDA
* Modeling
* Conclusion

### What can we find out about these countries about these top 25 countries for Female Life Expectency

#### Data To Collect

* Gun Violence (Guns/Citizen)
* Health Care (Is it universal healthcare)
* Obesity (Median BMI?)
* Drug Addition Rate
* Traffic Accidents
* Income Inequality (Gini coefficient)
* Population Size
* Education Level (Median Level)

| Country Name           | 2020            |
|------------------------|-----------------|
| "Hong Kong SAR, China" | 88.0            |
| "Macao SAR, China"     | 87.771          |
| Japan                  | 87.74           |                 
| "Korea, Rep."          | 86.5            |
| Singapore              | 86.1            |
| Bermuda                | 86.0            |
| French Polynesia       | 85.834          |
| Faroe Islands          | 85.5            |
| Australia              | 85.3            |
| France                 | 85.3            |
| Switzerland            | 85.2            |
| Spain                  | 85.1            |
| Finland                | 85.0            |
| Norway                 | 84.9            |
| Israel                 | 84.8            |
| Italy                  | 84.7            |
| Malta                  | 84.6            |
| New Caledonia          | 84.596          |
| Iceland                | 84.5            |
| Luxembourg             | 84.2            |
| Sweden                 | 84.2            |
| Euro area              | 84.190226427648 |
| Gibraltar              | 84.112          |
| Ireland                | 84.1            |
| Portugal               | 84.1            |
